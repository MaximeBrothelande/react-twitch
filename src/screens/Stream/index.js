import React from 'react'
import Navbar from '../../components/navbar'
import StreamLogo from './../../assets/img/lestream.png'

import styled from 'styled-components'
const StreamPage = () => {
  return (
    <Content>
      <Navbar />
      <StreamVideo
        src='https://player.twitch.tv/?channel=lestream&muted=true'
        height='210'
        width='100%'
        frameBorder='0'
        scrolling='no'
        allowFullScreen='true'
      ></StreamVideo>
      <StreamTitle>
        <Wrapper>
          <Image src={StreamLogo}></Image>
        </Wrapper>
      </StreamTitle>
      <StreamChat>Chat</StreamChat>
    </Content>
  )
}
export default StreamPage

const Content = styled.div`
  background-color: #eaeaea;
  height: 100%;
  min-height: 568px;
`
const StreamVideo = styled.iframe`
  background-color: red;
`
const StreamTitle = styled.div`
  background-color: blue;
  min-height: 68px;
`
const StreamChat = styled.div`
  background-color: purple;
  min-height: 268px;
`

const Wrapper = styled.div`
  background-color: purple;
  display: flex;
`
const Image = styled.img`
  height: 24px;
  width: 24px;
  border-radius: 12px;
`
