import React from 'react'
import Navbar from '../../components/navbar'
import Carousel from '../../components/carousel'

import styled from 'styled-components'

const HomePage = () => {
  return (
    <Content>
      <Navbar />

      <PageTitle>Bienvenue sur Twitch !</PageTitle>
      <Carousel title='Chaine live recommandées' />
      <Carousel title='Catégories recommandées' />
    </Content>
  )
}
const PageTitle = styled.h1`
  font-size: 24px;
  font-weight: bold;
  font-family: 'Arial Black', Gadget, sans-serif;
  padding: 10px 0;
`

const Content = styled.div`
  background-color: #eaeaea;
  height: 100%;
  min-height: 568px;
`
export default HomePage
