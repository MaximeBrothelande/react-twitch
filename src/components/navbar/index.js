import React from 'react'
import NavPoint from './navpoint.js'
import styled from 'styled-components'
import LogoTwitch from './../../assets/img/twitch.png'
import LogoClone from './../../assets/img/clone.png'
const NavBar = () => {
  return (
    <Wrapper>
      <UL>
        <NavPoint link='/' src={LogoTwitch} alt='TwitchLogo'></NavPoint>
        <NavPoint link='/directory' src={LogoClone} alt='DirLogo'></NavPoint>
        <NavPoint
          link='/lestream'
          src='src/assets/img/clone.png'
          alt='StreamLogo'
        ></NavPoint>
      </UL>
    </Wrapper>
  )
}

const Wrapper = styled.nav`
  height: 2em;
  background-color: whitesmoke;
  width: 100%;
  box-shadow: -4px 3px 5px 6px #ccc;
`
const UL = styled.ul`
  display: flex;
  justify-content: space-around;
`

export default NavBar
