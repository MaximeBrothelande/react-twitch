import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const NavPoint = ({ link, src, alt }) => {
  return (
    <div>
      <BulletPoint>
        <Link to={link}>
          <Image src={src} alt={alt}></Image>
        </Link>
      </BulletPoint>
    </div>
  )
}

NavPoint.propTypes = {
  src: PropTypes.string,
  link: PropTypes.string,
  alt: PropTypes.string
}
const Image = styled.img`
  height: 25px;
  width: 25px;
`
const BulletPoint = styled.li`
  padding: 0 0;
`

export default NavPoint
