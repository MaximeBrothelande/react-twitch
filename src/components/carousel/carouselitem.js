import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const CarouselItem = ({ label, link }) => {
  return (
    <CarouselItemDiv>
      <CarouselImg>img</CarouselImg>
      <Wrapper>
        <CarouselLogo />
        <Link to={link}>{label}</Link>
      </Wrapper>
    </CarouselItemDiv>
  )
}

CarouselItem.propTypes = {
  label: PropTypes.string,
  link: PropTypes.string
}

const CarouselItemDiv = styled.div`
  background-color: orange;
  height: 100px;
  min-width: 250px;
  margin-right: 10px;
  border: solid black 1px;
`

const CarouselImg = styled.div`
  background-color: red;
  height: 70%;
  min-width: 250px;
`

const CarouselLogo = styled.div`
  background-color: blue;
  height: 24px;
  width: 24px;
  border-radius: 12px;
`
const Wrapper = styled.div`
  background-color: green;
  display: flex;
`

export default CarouselItem
