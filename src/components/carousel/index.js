import React from 'react'
import CarouselItem from './carouselitem'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Carousel = ({ title }) => {
  return (
    <CarouselWrapper>
      <CarouselTitle>{title}</CarouselTitle>
      <CarouselDiv>
        <CarouselItem label='test' link='lestream'></CarouselItem>
        <CarouselItem label='test2' link='solary'></CarouselItem>
        <CarouselItem label='test3' link='lestream'></CarouselItem>
      </CarouselDiv>
    </CarouselWrapper>
  )
}

Carousel.propTypes = {
  title: PropTypes.string
}

const CarouselTitle = styled.h2`
  text-transform: uppercase;
  font-size: 10px;
  font-weight: bold;
  font-family: Arial, Helvetica, sans-serif;
`

const CarouselDiv = styled.div`
  display: flex;
  flex-direction: row;
  padding: 10px;
  overflow-x: scroll;
`

const CarouselWrapper = styled.div`
  margin: 0 10px;
`

export default Carousel
