import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
import HomeView from '../screens/Home/'
import StreamView from '../screens/Stream/'
const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={HomeView}></Route>
        <Route path='/:streamId' component={StreamView}></Route>
        <Redirect to='/'></Redirect>
      </Switch>
    </Router>
  )
}

export default Routes
